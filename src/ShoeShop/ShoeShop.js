import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import Modal from "./Modal";
import ProductList from "./ProductList";

export default class ShoeShop extends Component {
  state = {
    productList: dataShoe,
    cart: [],
    productDetail: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = this.state.cart;
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (id) => {
    if (window.confirm("Are you want to delete your product?") === true) {
      let cloneCart = this.state.cart;
      let index = cloneCart.findIndex((item) => {
        return item.id === id;
      });
      cloneCart.splice(index, 1);
      this.setState({ cart: cloneCart });
    }
  };
  handleChangeQuantity = (id, quantity) => {
    let cloneCart = this.state.cart;
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    if (quantity === 1) {
      cloneCart[index].quantity += quantity;
    } else {
      cloneCart[index].quantity > 1
        ? (cloneCart[index].quantity += quantity)
        : this.handleDelete();
    }
    this.setState({ cart: cloneCart });
  };
  handleDetail = (shoe) => {
    this.setState({ productDetail: shoe });
  };
  render() {
    return (
      <div>
        <h2>Shoes Shop</h2>
        <div className="row mt-0">
          <div className="col-6">
            <ProductList
              productList={this.state.productList}
              handleAddToCart={this.handleAddToCart}
              handleDetail={this.handleDetail}
            />
          </div>
          <div className="col-6 cart">
            <Cart
              handleDelete={this.handleDelete}
              cart={this.state.cart}
              handleChangeQuantity={this.handleChangeQuantity}
            />
          </div>
        </div>
        <Modal productDetail={this.state.productDetail} />
      </div>
    );
  }
}
