import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{(index += 1)}</td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, -1)}
              className="btn btn-danger mr-1"
            >
              -
            </button>
            {item.quantity}
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, 1)}
              className="btn btn-success ml-1"
            >
              +
            </button>
          </td>
          <td>{item.price * item.quantity}$</td>
          <td>
            <button
              onClick={() => this.props.handleDelete(item.id)}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h2>Cart</h2>
        <table className="table">
          <thead>
            <tr>
              <th>STT</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
