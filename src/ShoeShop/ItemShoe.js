import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-4 my-3">
        <div className="card" style={{ height: "100%" }}>
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h6>{price}$</h6>
            <button
              onClick={() => this.props.handleAddToCart(this.props.item)}
              className="btn btn-primary my-2"
            >
              Add to cart
            </button>
            <button
              onClick={() => this.props.handleDetail(this.props.item)}
              data-toggle="modal"
              data-target="#exampleModal"
              className="btn btn-success"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
