import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ProductList extends Component {
  render() {
    return (
      <div className="row mt-0">
        {this.props.productList.map((item, index) => {
          return (
            <ItemShoe
              key={index}
              item={item}
              handleAddToCart={this.props.handleAddToCart}
              handleDetail={this.props.handleDetail}
            />
          );
        })}
      </div>
    );
  }
}
